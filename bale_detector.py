#d2wF9Le2R57pJVt4yNRZ

import jetson.inference
import jetson.utils
import serial
import time
import pexpect

# Change permissions automatically to be able to access UART port
child = pexpect.spawn('sudo chmod 666 /dev/ttyTHS1')
child.expect('password')
child.sendline('jetson')
print(child.read())

serial_port = serial.Serial(
    port="/dev/ttyTHS1",
    baudrate=9600,
    bytesize=serial.EIGHTBITS,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
)
# Wait a second to let the port initialise
time.sleep(1)

net = jetson.inference.detectNet(argv=['--model=nano/baleCollectorRobot/models/bales2/ssd-mobilenet.onnx', '--labels=nano/baleCollectorRobot/models/bales2/labels.txt', '--input-blob=input_0', '--output-cvg=scores', '--output-bbox=boxes'])
camera = jetson.utils.videoSource("csi://0")      # '/dev/video0' for V4L2
display = jetson.utils.videoOutput("display://0") # 'my_video.mp4' for file
tolerance = 50
turn = 0

serial_port.write("Ready to go\r\n".encode())

while display.IsStreaming():
	try:
		img = camera.Capture()
	except:
		print("!!!!!!!!!!!!!!!!!!!!!!!!!Error!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\n\n")
		
	detections = net.Detect(img)
	display.Render(img)
	display.SetStatus("Object Detection | Network {:.0f} FPS".format(net.GetNetworkFPS()))

	if (len(detections)>0):
		center = detections[0].Center
		frameCenter = (camera.GetWidth())/2
		pos = center[0] - frameCenter
		#print(pos
		bale_pos = round((center[0]/camera.GetWidth())*200)
		print(bale_pos)
		
		
		serial_port.write((str(bale_pos)+"\n").encode())
